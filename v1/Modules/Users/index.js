const { admin, nodemailer } = require("../../../admin")

class Users{

    constructor(firebase){
        this.profiles = firebase.firestore().collection('PROFILES')
        const responses = require('../../../json/v1/responses.json')

        this.add = (data) => {
            // console.log(data)
            return admin.firestore().runTransaction(t => {
                const promises = []
                data.usersList.map(user => {
                    // console.log(user)
                    user.serverTimestamp = data.metadata.serverTimestamp
                    user.timestamp = data.metadata.timestamp
                    user.createdBy = data.metadata.uid
                    
                    const doc = this.profiles.doc(user.uid)
                    return promises.push(t.set(doc, user,{merge:true}))
                })
                return Promise.all(promises)
            })
        }


        this.listUsers = (data) => {
            // console.log(data)

            // Filters need to be added
            const query_uid = data.query_uid ? data.query_uid.toUpperCase() : ""
            const query_name = data.query_name ? data.query_name : ""
            const query_email = data.query_email ? data.query_email.toLowerCase() : ""
            const query_mobile = data.query_mobile ? data.query_mobile : ""

            const previous = data.previous || 0
            const limit = data.limit || 10

            // const course = data.course || ""
            // const branch = data.branch || ""
            // const year = data.year || ""
            // const semester = data.semester || ""
            // const section = data.section || ""
            // const academicYear = data.academicYear || ""
            
            // const docRef = [admin.firestore().collection("ACADEMICS")]
            let queryRef = admin.firestore().collection("USERS")
            if(query_uid)
            queryRef = queryRef.where("uid",">=",query_uid).where("uid","<=",query_uid+'\uf8ff')
            else if(query_name)
            queryRef = queryRef.where("displayName",">=",query_name).where("displayName","<=",query_name+'\uf8ff')
            else if(query_email)
            queryRef = queryRef.where("email",">=",query_email).where("email","<=",query_email+'\uf8ff')
            else if(query_mobile)
            queryRef = queryRef.where("phoneNumber",">=",query_mobile).where("phoneNumber","<=",query_mobile+'\uf8ff')
            else
            queryRef = queryRef.where("uid",">","")

            if(previous)
                queryRef = queryRef.startAfter(previous)
            queryRef = queryRef.limit(limit)
            
            // console.log("started")
            return queryRef.get()
            .then(snapshotList => {
                const promises = []
                snapshotList.docs.forEach(doc => {
                    const user = doc.data()
                    // console.log(data)
                    promises.push(user)
                    
                })
                promises.map(user => console.log(user.uid))
                return Promise.all(promises)
            }).catch(err => {
                throw err
            })
        }
        // this.listUsers({})

        this.resetPassword = (data) => {
            return this.profiles.where("uid","==",data.uid).where("accountCreated","==",true).get()
            .then(snapshot => {
                if(snapshot.size < 1)
                    throw new Error("not-found")
                const users = []
                snapshot.forEach(user => {
                    users.push(user.data())
                })
                // console.log(data.password)
                return admin.auth().updateUser(data.uid,{
                    password : data.password || users[0].email
                })
            }).catch(err => {
                throw err
            })
        }

        this.deactivate = (data) => {
            const promises = data.users.map(user => {
                return admin.auth().updateUser(user,{
                    disabled : true
                })
            })
            return Promise.all(promises)
        }

        this.activate = (data) => {
            const promises = data.users.map(user => {
                return admin.auth().updateUser(user, {
                    disabled: false
                })
            })
            return Promise.all(promises)
        }

        this.listFailed = () => {
            return this.profiles.where("accountCreated","==",false).get()
            .then(docList => {
                // console.log(docList)
                const promises = []
                docList.docs.forEach(doc => promises.push(doc.data()))
                return Promise.all(promises)
            }).then(res => {
                console.log(res)
                return res
            }).catch(err => {
                console.log(err)
                return []
            })
        }

        this.fixFailed = (data) => {
            
            return this.profiles.where("accountCreated", "==", true).where("uid", "==", data.uid.toUpperCase()).get()
            .then(docs => {
                if (docs.size !== 0) {
                    throw new Error("exists")
                }
                return this.profiles.where("accountCreated", "==", false).where("uid","==",data.uid).get()
            }).then(docs => {
                    console.log(docs)
                    if (res.size < 1)
                        throw new Error("not-found")
                    let promise = null
                    // Error here need to fix
                    docs.foreach(doc => {
                        promise = doc.data()
                    })
                    return Promise.resolve(promise)
                }).then(res => {
                    return createUserRecord(res)
                }).catch(err => {
                    console.log(err)
                    throw err
                })
        }

        this.createUserRecord = (inputData) => {
                const email = inputData.uid.toLowerCase() + "@aaa.automation";
                // console.log('createUserRecord')
                if (!inputData.uid)
                    return admin.firestore().collection('PROFILES').doc(inputData.uid).set({
                        error: "UID CANNOT BE NULL"
                    }, { merge: true })
                if (!email)
                    return admin.firestore().collection('PROFILES').doc(inputData.uid).set({
                        error: "EMAIL CANNOT BE NULL"
                    }, { merge: true })
                const data = {
                    displayName: inputData.displayName,
                    uid: inputData.uid.toUpperCase(),
                    email: email.toLowerCase(),
                    password: email.toLowerCase(),
                    // phoneNumber: inputData.phoneNumber,
                    emailVerified: false,
                    photoURL: "https://firebasestorage.googleapis.com/v0/b/site-org-automation.appspot.com/o/APPLICATION%2FV1%2Fdefault%20profile%20image.jpg?alt=media&token=38fc162a-91be-4f5b-a719-46d79b80ec09"
                }
                // console.log(data)
                return admin.firestore().collection('USERS').where('uid', '==', data.uid).get()
                    //    .then(res => {
                    //         if (res.size > 0)
                    //             throw new Error('exists')
                    //         return admin.firestore().collection('USERS').where('phoneNumber', '==', data.phoneNumber).get()})
                    .then(res => {
                        if (res.size > 0)
                            return admin.firestore().collection('PROFILES').doc(data.uid).set({
                                error: "UID ALREADY TAKEN"
                            }, { merge: true })
                        return admin.firestore().collection('USERS').where('email', '==', data.email).get()
                    })
                    .then(res => {
                        if (res.size > 0)
                            return admin.firestore().collection('PROFILES').doc(inputData.uid).set({
                                error: "EMAIL ALREADY TAKEN"
                            }, { merge: true })
                        const phoneNumberList = []
                        if(inputData.phoneNumber)
                            phoneNumberList.push(inputData.phoneNumber)
                        if(inputData.fphoneNumber)
                            phoneNumberList.push(inputData.fphoneNumber)
                        if(inputData.mphoneNumber)
                            phoneNumberList.push(inputData.mphoneNumber)
                        return admin.firestore().collection('USERS').doc(data.uid).set({
                            displayName: data.displayName || "",
                            uid: data.uid,
                            photo : {
                                sourcePath: "gs://site-org-automation.appspot.com/APPLICATION/V1/default profile image.jpg",
                                photoURL : data.photoURL || ""
                            },
                            phoneNumberList: phoneNumberList,
                            emailList: inputData.email ? [inputData.email] : [],
                            claims: {
                                roles: ["user"]
                            },
                            education : {
                                ssc : {
                                    institution : {
                                        name : inputData.ssc_institute_name || "",
                                        address : inputData.ssc_institute_address || ""
                                    },
                                    score : inputData.ssc_score || ""
                                },
                                intermediate : {
                                    institution : {
                                        name : inputData.intermediate_institute_name || "",
                                        address : inputData.intermediate_institute_address || ""
                                    },
                                    score : inputData.intermediate_score || "",
                                    eamcet : inputData.eamcet_score || "",
                                    group : inputData.intermediate_group || ""
                                }
                            },
                            address : {
                                permanent : {
                                    houseNumber : inputData.paddress_houseNumber || "",
                                    street : inputData.paddress_street || "",
                                    landmark : inputData.paddress_landmark || "",
                                    city : inputData.paddress_city || "",
                                    state : inputData.paddress_state || "",
                                    country : inputData.paddress_country || "",
                                    pincode : inputData.paddress_pincode || ""
                                },
                                present : {
                                    houseNumber : inputData.caddress_houseNumber || "",
                                    street : inputData.caddress_street || "",
                                    landmark : inputData.caddress_landmark || "",
                                    city : inputData.caddress_city || "",
                                    state : inputData.caddress_state || "",
                                    country : inputData.caddress_country || "",
                                    pincode : inputData.caddress_pincode || ""
                                }
                            },
                            admission : {
                                doj : inputData.doj || "",
                                admissionNumber : inputData.admissionNumber || "",
                                seatType : inputData.seatType || ""
                            },
                            academics:{
                                batch : inputData.batch || "",
                                course : inputData.course || "",
                                branch : inputData.branch || "",
                                year : inputData.year || "",
                                semester : inputData.semester || ""
                            },
                            personal : {
                                gender : inputData.gender || "",
                                dob : inputData.dob || "",
                                socialId : inputData.socialId || "",
                                nationality : inputData.nationality || "",
                                caste : inputData.caste || "",
                                religion : inputData.religion || "",
                                father : {
                                    name : inputData.fname || "",
                                    occupation : inputData.foccupation || "",
                                    phoneNumber : inputData.fphoneNumber || ""
                                },
                                mother : {
                                    name : inputData.mname || "",
                                    occupation : inputData.moccupation || "",
                                    phoneNumber : inputData.mphoneNumber || ""
                                }
                            },
                            serverTimestamp: new Date().toUTCString()
                        })
                    }).then(user => {
                        // console.log(user)
                        return admin.auth().createUser(data)
                    }).then(res => {
                        delete data.password;
                        return admin.firestore().collection('PROFILES').doc(data.uid).set({
                            ...data,
                            inputData : { ...inputData },
                            serverTimestamp : new Date().toUTCString(),
                            accountCreated: true,
                            validated : true
                        })
                    }).catch(err => {
                        throw err
                    })
        }
    }
    
}

module.exports = Users