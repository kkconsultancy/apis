// console.log("index.js started")
const functions = require('firebase-functions')
const cors = require('cors')({ origin: true })

const {admin,app} = require("./admin")

const responses = require('./json/v1/responses.json')

new (require('./v1/public'))()
new (require('./v1/closed'))()
new (require('./v1/privileged'))()

exports.api = functions.https.onRequest(app)

app.get("/addMaterials.html",(request,response) => {
    console.log(request.originalUrl)
    
    return response.sendFile("./v1/Modules/Materials/index.html", {root: __dirname })
})
app.post("/addMaterials.html",(request,response) => {
    console.log(request.originalUrl)
    
    return response.sendFile("./v1/Modules/Materials/index.html", {root: __dirname })
})

exports.helloWorld = functions.https.onRequest((request, response) => {
    console.log('/helloWorld')
    if (request.method !== 'POST') {
        return response.status(200).send({data : 'HOW ON EARTH YOU EXISTS...'})
    }
    return response.send({data : "Hello from Firebase!"});
});

const users = new (require('./v1/Modules/Users'))(admin)
exports.newUserListener = functions.firestore.document("PROFILES/{user}").onCreate((change, context) => {
    const inputData = change.data()
    if(inputData.validated)
        return "DUPLICATION IGNORED";
    console.log('newUserListener --> '+ inputData.uid)
    console.log(inputData)
    if(inputData.accountCreated)
        throw new Error("users/account-exists")
    // console.log(admin.firestore()._settings.servicePath)
    return admin.firestore().collection('PROFILES').doc(inputData.uid).set({
        accountCreated : false,
        uid : inputData.uid,
        validated : true
    }, { merge: true }).then(res => {
        return users.createUserRecord(inputData)
    }).then(res => {
        return Promise.resolve(res)
    }).catch(err => {
        // throw err
        console.error(err)
    })
})

exports.getFirebaseConfig = functions.https.onRequest((request, response) => cors(request, response, () => {
    console.log('/getFirebaseConfig')
    const clientConfig = require('./json/v1/client_key.json')
    clientConfig.branding = responses.branding
    let messages = responses["awesome-words"] 
    const conditions = { hostname: request.hostname, type: request.method, parameters: request.body }
    // console.log(conditions)

    const msg = messages[(Math.random()*1000) % messages.length]

    if (conditions.type !== 'POST') {
        console.log(conditions);
        return response.status(200).send({ data: msg, branding: responses.branding})
    }
    else if (conditions.parameters.password !== "siteautomation.developerswork.online") {
        console.log(conditions);
        return response.status(200).send({ data: msg, branding: responses.branding})
    }
    return response.status(200).send(clientConfig)
}));

// admin.firestore().collection("PROFILES").get()
// .then(collectionSnapshots => {
//     // console.log(snap)
//     collectionSnapshots.forEach(doc => {
//         console.log(doc.data())
//     })
//     console.log("DONE")
// }).catch(err => {
//     console.log(err)
// })

// admin.firestore().collection("MARKS").doc("2019-2020").listCollections()
// .then(snap => {
//     // console.log(snap)
//     return Promise.all(snap.map(c => c.listDocuments()))
// }).then(d => {
//     console.log(d)
//     d.map(d => d.map(d => {
//         console.log(d.id)
//         return d
//     }))
// })

// const data = (require("./json/v1/template.json")).SERVICES["ACADEMICS MANAGEMENT"]
// console.log(data)
// admin.firestore().collection("SERVICES").doc("ACADEMICS MANAGEMENT").set({...data}).then(() => console.log("done")).catch(err => console.log(err))

    // let data = (require("./json/v1/template.json")).SERVICES["MARKS MANAGEMENT"]
    // console.log(data)
    // admin.firestore().collection("SERVICES").doc("MARKS MANAGEMENT").set({...data},{merge:true})

    // admin.firestore().collection("ROLES").doc("administrator").get().then(doc => console.log(doc.data())).catch(err => console.log(err))
// const data = (require("./json/v1/template.json")).ROLES["administrator"]    
// admin.firestore().collection("ROLES").doc("administrator").set(data).then(doc => console.log("done")).catch(err => console.log(err))
