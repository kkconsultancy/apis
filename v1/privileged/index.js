const {admin,privilegedAPI,nodemailer} = require("../../admin")

// console.log("Privilaged Started")
class PrivilegedAPI {

    constructor() {
        const app = privilegedAPI
        const responses = require('../../json/v1/responses.json')
        const Users = new (require('../Modules/Users'))(admin)
        const Attendance = new (require('../Modules/Attendance'))(admin)
        const Academics = new (require('../Modules/Academics'))()
        const Hostel = new (require("../Modules/Hostel"))()
        const Transport = new (require("../Modules/Transport"))()
        const Marks = new (require("../Modules/Marks"))()
        const Services = new (require("../Modules/Services"))()
        const Roles = new (require("../Modules/Roles"))()
        const Placements = new (require("../Modules/Placements"))()
        const Materials = new(require("../Modules/Materials"))()
        const smtp = nodemailer;
        // const academics = new Academics()

        app.post("/uploadMaterials",(request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }
            let token_info = {}
            return Promise.all([])
            // admin.auth().verifyIdToken(token).then(res => {
            //     if (res.user_id !== uid)
            //         throw new Error('auth')
            //     token_info = res
            //     // console.log(res)
            //     // return admin.firestore().collection('USERS').doc(uid).get()
            //     // return response.send({ res: res })
            //     let flag = 0
            //     // for (let i in token_info.services["USERS MANAGEMENT"]) {
            //     //     if (token_info.services["USERS MANAGEMENT"][i] === "update") {
            //     //         flag = 1;
            //     //     }
            //     // }
            //     // if (flag === 0)
            //     //     throw new Error('auth')

            //     return flag
            // })
            .then(res => {
                return Materials.add({...request.body})
            }).then(() => {
                return response.send({ status : true, branding : responses.branding })
            }).catch(err => {
                console.log(err)
                if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                    // console.log('user id wrong')
                    return response.status(200).send(responses["auth-error"].general)
                }
                if (err.toString().match("not-found") ) {
                    // console.log('user id wrong')
                    return response.status(200).send(responses["data-not-found-error"].general)
                }
                if (err.toString().match("exists")) {
                    // console.log(responses["data-exists"])
                    return response.status(200).send(responses["data-exists"].general)
                }
                return response.status(200).send(responses["server-error"].general)
            })
        })

        app.post('/generateLoginToken', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const to = request.body.to
            const claims = request.body.claims

            if (!uid || !token || uid !== originalUrlUid || !to || !claims) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
            .then(res => {
                if (res.user_id !== uid)
                    throw new Error('auth')
                token_info = res
                // claims.uid = token_info.uid
                return admin.auth().createCustomToken(to, claims)
                // return response.send({ res: res })
            }).then(token => {
                const mailConfig = {
                    from : token_info.name+" <"+uid+"@aaa.automation.proj>",
                    to : to,
                    subject : "KEY TO LOGIN AS "+token_info.user_id,
                    html : "<h5>This key will be valid for only 1 hour</h5><br/>Key:<h1>"+token+"</h1>"
                };
                console.log(mailConfig)
                return smtp.sendMail(mailConfig)
            }).then(res => {
                return response.send({ status : true })
            }).catch(err => {
                console.log(err)
                if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                    // console.log('user id wrong')
                    return response.status(200).send(responses["auth-error"].general)
                }
                return response.status(200).send(responses["server-error"].general)
            });
        });

        app.post('/resetPassword', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const user = request.body.id
            const password = request.body.password

            if (!uid || !token || uid !== originalUrlUid || !user || !password) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["USERS MANAGEMENT"]) {
                        if (token_info.services["USERS MANAGEMENT"][i] === "update") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Users.resetPassword({
                        uid : user,
                        password : password
                    })
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status : true })
                }).catch(err => {
                    // console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/deactivateAccounts', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.usersList

            if (!uid || !token || uid !== originalUrlUid || !users || !Array.isArray(users)) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["USERS MANAGEMENT"]) {
                        if (token_info.services["USERS MANAGEMENT"][i] === "deactivate") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Users.deactivate({
                        users : users
                    })
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/activateAccounts', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.usersList

            if (!uid || !token || uid !== originalUrlUid || !users || !Array.isArray(users)) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["USERS MANAGEMENT"]) {
                        if (token_info.services["USERS MANAGEMENT"][i] === "activate") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Users.activate({
                        users : users
                    })
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/addUserAccounts', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.users
            const timestamp = request.body.timestamp

            if (!uid || !token || uid !== originalUrlUid || !users || !Array.isArray(users) || users.length < 1 || !timestamp) {
                return response.status(200).send(responses["request-error"].general)
            }

            const serverTimestamp = new Date().toUTCString()

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for(let i in token_info.services["USERS MANAGEMENT"]){
                        if (token_info.services["USERS MANAGEMENT"][i] === "create"){
                            flag = 1;
                        }
                    }
                    if(flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return Users.add({
                        usersList : users,
                        metadata : {
                            timestamp : timestamp,
                            serverTimestamp : serverTimestamp,
                            uid : uid
                        }
                    })
                }).then(res => {
                    console.log(res)
                    // return console.log(Users)
                    return response.send({ processing: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/getBrokenUserAccounts', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid ) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.uid !== uid)
                        throw new Error('auth')
                    token_info = res
                    console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    console.log(res.services)
                    let flag = 0
                    for (let i in token_info.services["USERS MANAGEMENT"]) {
                        if (token_info.services["USERS MANAGEMENT"][i] === "update") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    console.log(res)
                    // return console.log(Users)
                    return Users.listFailed()
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send(res)
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/recreateAccount', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]
            const id = request.body.id

            if (!uid || !token || uid !== originalUrlUid || !id) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["USERS MANAGEMENT"]) {
                        if (token_info.services["USERS MANAGEMENT"][i] === "update") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    console.log(res)
                    // return console.log(Users)
                    return Users.fixFailed({uid : id})
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send(res)
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found") ) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/listUsers', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.uid !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["USERS MANAGEMENT"]) {
                        if (token_info.services["USERS MANAGEMENT"][i] === "view") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return Users.listUsers({...request.body})
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send(res)
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/viewAttendance', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ATTENDANCE MANAGEMENT"]) {
                        if (token_info.services["ATTENDANCE MANAGEMENT"][i] === "view") {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Attendance.view({...request.body})
                }).then(res => {
                    return response.send(res || {})
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/postAttendance', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ATTENDANCE MANAGEMENT"]) {
                        if (token_info.services["ATTENDANCE MANAGEMENT"][i] === "create") {
                            flag = 1;
                            break;
                        }
                        if (token_info.services["ATTENDANCE MANAGEMENT"][i] === "update") {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Attendance.add({ ...request.body })
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/getAttendance', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.uid !== uid)
                        throw new Error('auth')
                    token_info = res
                    return Attendance.get({ ...request.body })
                }).then(res => {
                    return response.send({
                        absent : res
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/postMarks', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.uid !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["MARKS MANAGEMENT"]) {
                        if (token_info.services["MARKS MANAGEMENT"][i] === "create") {
                            flag = 1;
                            break;
                        }
                        if (token_info.services["MARKS MANAGEMENT"][i] === "update") {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Marks.add({ ...request.body })
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/viewMarks', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["MARKS MANAGEMENT"]) {
                        if (token_info.services["MARKS MANAGEMENT"][i] === "view") {
                            flag = 1;
                            break;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Marks.view({...request.body})
                }).then(res => {
                    return response.send(res || {})
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/getMarks', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.uid !== uid)
                        throw new Error('auth')
                    token_info = res
                    return Marks.get({ ...request.body })
                }).then(res => {
                    return response.send(res)
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/mapStudentsToAcademics', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "assign") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.mapStudentsToAcademics({...request.body})
                }).then(res => {
                    // console.log(res)
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/addSubjectsoAcademics', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "create") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.addSubjectsoAcademics({...request.body})
                }).then(res => {
                    // console.log(res)
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/duplicateStudentsToAcademics', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "assign") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.duplicateStudentsToAcademics({...request.body})
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/mapSubjectsToAcademics', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "assign") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.mapSubjectsToAcademics({...request.body})
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/listStudents', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "view") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.listStudents({...request.body})
                }).then(res => {
                    return response.send(res)
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/addNewCourse', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "create") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.addNewCourse({...request.body})
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/addNewSpecialization', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "create") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.addNewSpecialization({...request.body})
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/removeSpecilization', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "create") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.removeSpecilization({...request.body})
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/removeCourse', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "create") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.removeCourse({...request.body})
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/duplicateAcademicYearInformation', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "create") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.duplicateAcademicYearInformation({...request.body})
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });
        
        app.post('/changeAcademicYear', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["ACADEMICS MANAGEMENT"]) {
                        if (token_info.services["ACADEMICS MANAGEMENT"][i] === "update") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Academics.changeAcademicYear({...request.body})
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/addStudentsToHostel', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["HOSTEL MANAGEMENT"]) {
                        if (token_info.services["HOSTEL MANAGEMENT"][i] === "add") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Hostel.add({...request.body})
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/addStudentsToTransport', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }

            let token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    let flag = 0
                    for (let i in token_info.services["TRANSPORT MANAGEMENT"]) {
                        if (token_info.services["TRANSPORT MANAGEMENT"][i] === "add") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Transport.add({...request.body})
                }).then(res => {
                    return response.send({
                        status : true
                    })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    }
                    if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].users)
                    } if (err.toString().match("incorrect-parameters"))
                        return response.status(200).send(responses["request-error"].general)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/deactivateService', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.usersList

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["SERVICES MANAGEMENT"]) {
                        if (token_info.services["SERVICES MANAGEMENT"][i] === "deactivate") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Services.deactivate({...request.body})
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/activateSevice', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.usersList

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["SERVICES MANAGEMENT"]) {
                        if (token_info.services["SERVICES MANAGEMENT"][i] === "activate") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Services.activate({...request.body})
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/createRole', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.usersList

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["ROLES MANAGEMENT"]) {
                        if (token_info.services["ROLES MANAGEMENT"][i] === "create") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Roles.create({...request.body})
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/mapServicesToRole', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.usersList

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["ROLES MANAGEMENT"]) {
                        if (token_info.services["ROLES MANAGEMENT"][i] === "map") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Roles.map({...request.body})
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/mapUsersToRole', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.usersList

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["ROLES MANAGEMENT"]) {
                        if (token_info.services["ROLES MANAGEMENT"][i] === "assign") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Roles.assign({...request.body})
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/createPlacement', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.usersList

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["PLACEMENTS MANAGEMENT"]) {
                        if (token_info.services["PLACEMENTS MANAGEMENT"][i] === "create") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Placements.create({...request.body})
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/hostelOutingGranted', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.usersList

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["HOSTEL MANAGEMENT"]) {
                        if (token_info.services["HOSTEL MANAGEMENT"][i] === "outing") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Hostel.outingGranted({...request.body})
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });
        
        app.post('/addStudentsToPlacement', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            const users = request.body.usersList

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    // console.log(res)
                    // return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                    let flag = 0
                    for (let i in token_info.services["PLACEMENTS MANAGEMENT"]) {
                        if (token_info.services["PLACEMENTS MANAGEMENT"][i] === "add") {
                            flag = 1;
                        }
                    }
                    if (flag === 0)
                        throw new Error('auth')

                    return flag
                }).then(res => {
                    return Placements.add({...request.body})
                }).then(res => {
                    // console.log(res)
                    // return console.log(Users)
                    return response.send({ status: true })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

    }

}

module.exports = PrivilegedAPI;