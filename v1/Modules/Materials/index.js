const {admin} = require('../../../admin')
const materials = admin.firestore().collection("MATERIALS")

class Materials {

    constructor() {

        this.add = (data) => {
            // drive, training
            console.log(data)
            if(!data.uid || !data.course || !data.class || !data.subject || !data.topicName || !data.topicLink)
                throw new Error("incorrect-parameters")

            const docRef = materials.doc()

            const buffer = {
                uid : data.uid,
                course : data.course,
                serverTimestamp : new Date().toUTCString(),
                topic : {
                    name : data.topicName,
                    link : data.topicLink
                },
                class : data.class,
                id : docRef.id,
                subject : data.subject,
                section : data.section || "-"
            }
            return materials.where("topic.link","==",data.topicLink).get()
            .then(docsSnapshot => {
                if(docsSnapshot.size)
                    throw new Error("exists")
                return docRef.set(buffer)
            }).catch(err => {
                throw err
            })
        }

        this.get = (data) => {
            // eligible(drive, training), placed, shortlisted           
            let docRef = materials.where("class","==",data.class)
            if(data.section)
                materials.where("section","==",data.section)
            if(data.subject)
                materials.where("subject","==",data.subject)

            return docRef.get()
            .then(docsSnapshot => {
                const promises = []
                docsSnapshot.forEach(doc => {
                    promises.push(doc.data())
                })
                return Promise.all(promises)
            }).catch(err => {
                throw err
            })
        }
    }

}

module.exports = Materials