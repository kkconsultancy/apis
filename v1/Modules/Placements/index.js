const {admin} = require('../../../admin')
const placements = admin.firestore().collection("PLACEMENTS")

class Placements {

    constructor() {

        this.create = (data) => {
            // drive, training
            if(!data.type || !data.name || !data.startsAt || !data.endsAt)
                throw new Error("incorrect-parameters")
            const docRef = placements.doc()

            const buffer = {
                type : data.type,
                serverTimestamp : new Date().toUTCString(),
                duration : {
                    start : data.startsAt,
                    end : data.endsAt
                },
                id : docRef.id,
                name : data.name
            }

            return docRef.set(buffer)
        }

        this.add = (data) => {
            // eligible(drive, training), placed, shortlisted
            if(!data.id || !data.studentsList || !data.type)
                throw new Error()

            return placements.doc(data.id).get()
            .then(snapshot => {
                if(!snapshot.exists)
                    throw new Error("not-found")
                const inputData = snapshot.data()
                if(data.type === "eligible")
                    inputData.eligible = data.studentsList
                if(data.type === "shortlisted"){
                    if(!data.shortlisted)
                        data.shortlisted = []
                    inputData.shortlisted.push(data.studentsList)
                }if(data.type === "placed"){
                    inputData.placed = data.studentsList
                }
                
                return placements.doc(data.id).set(inputData)
            }).catch(err => {
                throw err
            })
        }
    }

}

module.exports = Placements