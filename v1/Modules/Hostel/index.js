const {admin} = require("../../../admin")
const hostel = admin.firestore().collection("HOSTEL")
const adminstration = admin.firestore().collectionGroup("Administration")

class Hostel{

    constructor(){

        this.create = (data) => {
            // Need to manage
        }

        this.add = (data) => {
            if(!data.academicYear || !data.course || !data.branch || !data.year)
                throw new Error("incorrect-parameters")
            if(!data.studentsList || !Array.isArray(data.studentsList))
                throw new Error("incorrect parameters")

            const promises = []
            const hostelRef = hostel.doc(data.academicYear).collection(data.course+"/"+data.branch+"/"+data.year)
            return admin.firestore().runTransaction(t => {
                data.studentsList.map(student => {
                    const studentData = {
                        contact : {
                            address : student.address || "",
                            phoneNumber : student.phoneNumber || ""
                        },room : {
                            category : student.category || "",
                            code : student.room_code || "",
                            type : student.room_type || ""
                        },
                        metadata : {
                            course : data.course,
                            academicYear : data.academicYear,
                            branch : data.branch,
                            year : data.year,
                            serverTimestamp : new Date().toUTCString()
                        },
                        uid : student.uid
                    }
                    const promise = t.set(hostelRef.doc(student.uid),studentData)
                    promises.push(promise)

                    return student
                })
    
                return Promise.all(promises)
            })
        }

        this.outingGranted = (data) => {
            if(!data.uid || !data.student || !data.on)
                throw new Error("incorrect-parameters")

            const hostelRef = hostel.doc(data.student).collection("outing").doc()
            const outingDetails = {
                grandetBy : data.uid,
                serverTimestamp : new Date().toUTCString(),
                grantedOn : data.on,
                id : hostelRef.id
            }
            return hostelRef.set(outingDetails)
        }
    }
    
}

module.exports = Hostel