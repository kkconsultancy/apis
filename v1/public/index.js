const {admin,publicAPI} = require("../../admin")

class PublicAPI {
    constructor() {
        const app = publicAPI
        const responses = require('../../json/v1/responses.json')
        const Materials = new(require("../Modules/Materials"))()

        app.post('/getMaterials',(request,response) => {
            console.log(request.originalUrl)
            
            if (!request.body.class) {
                return response.status(200).send(responses["request-error"].general)
            }

            return Materials.get(request.body)
            .then(res => {
                console.log(res)
                return response.send({
                    materials : res,
                    branding : responses.branding
                })
            })
            .catch(err => {
                console.log(err)
                if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                    // console.log('user id wrong')
                    return response.status(200).send(responses["auth-error"].general)
                }
                if (err.toString().match("not-found") ) {
                    // console.log('user id wrong')
                    return response.status(200).send(responses["data-not-found-error"].general)
                }
                if (err.toString().match("exists")) {
                    // console.log('user id wrong')
                    return response.status(200).send(responses["data-exists"].general)
                }
                return response.status(200).send(responses["server-error"].general)
            })
        })

        app.post('/isPhoneNumberAvailable',(request,response) => {
            console.log(request.originalUrl)
            const phoneNumber = request.body.phoneNumber
            
            if (!phoneNumber || !phoneNumber.match(/^\+[0-9]{1,}[0-9]{10}$/)){
                return response.status(200).send(responses["incorrect-parameters"].general)
            }

            return admin.firestore().collection("USERS").where("phoneNumberList", "array-contains", phoneNumber).get()
            .then(res => {
                if(res.size < 1)
                    return response.send({exists:false})
                else
                    return response.send({exists:true})
            })
            .catch(err => {
                console.log(err)
                return response.status(200).send(responses["server-error"].general)
            })
        });

        app.post('/isEmailAvailable', (request, response) => {
            console.log(request.originalUrl)
            const email = request.body.email ? request.body.email.toLowerCase() : ""

            if (!email || !email.match(/^.+@.+\..+$/)) {
                return response.status(200).send(responses["incorrect-parameters"].general)
            }

            return admin.firestore().collection("USERS").where("emailList", "array-contains", email).get()
                .then(res => {
                    if (res.size < 1)
                        return response.send({ exists: false })
                    else
                        return response.send({ exists: true })
                })
                .catch(err => {
                    console.log(err)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/isUidAvailable', (request, response) => {
            console.log(request.originalUrl)
            const uid = request.body.uid ? request.body.uid.toLowerCase() : ""

            if (!uid) {
                return response.status(200).send(responses["incorrect-parameters"].general)
            }

            return admin.firestore().collection("USERS").where("uid", "==", uid).get()
                .then(res => {
                    if (res.size < 1)
                        return response.send({ exists: false })
                    else
                        return response.send({ exists: true })
                })
                .catch(err => {
                    console.log(err)
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post("/sendOTP",(request,response) => {})

        app.post('/getMeta',(request,response) => {
            console.log(request.originalUrl)
            // const metadata = require('../../json/v1/metadata.json')
            admin.database().ref('METADATA').once('value')
            .then(res => {
                // console.log(res)
                // metadata = res.val()
                // console.log(res.val())
                return response.send(res.val())

            }).catch(err => {
                console.log(err)
                return response.status(200).send(responses["server-error"].general)
            })

        })
    
    }

}

module.exports = PublicAPI;