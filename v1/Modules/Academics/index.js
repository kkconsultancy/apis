const {admin} = require("../../../admin")

const academics = admin.firestore().collection("ACADEMICS")

class Academics {

    constructor() {

        this.duplicateAcademicYearInformation = (data) => {
            if(!data.fromAcademicYear || !data.toAcademicYear)
                throw new Error("incorrect-parameters")
            const academicYear = data.toAcademicYear

            return admin.firestore().collection("METADATA").doc(data.fromAcademicYear).get()
            .then(snapshot => {
                if(!snapshot.exists)
                    throw new Error("not-found")
                const data = snapshot.data()
                console.log(data)
                return admin.firestore().collection("METADATA").doc(academicYear).set({
                    courses : data.courses,
                    year : academicYear
                })
            }).catch(err => {
                throw err
            })
        }

        this.changeAcademicYear = data => {
            if(!data.academicYear)
                throw new Error("incorrect-parameters")

            return admin.firestore().collection("METADATA").doc("institution").set({
                academicYear : data.academicYear
            },{merge : true})
            .then(() => {
                return admin.firestore().collection("METADATA").doc(data.academicYear).get()
            })
            .then((snapshot) => {
                const courses = snapshot.data().courses
                return admin.database().ref("METADATA").set({
                    year : data.academicYear,
                    courses : courses
                })
            }).catch(err => {
                throw err
            })
        }

        this.listAcademicYears = () => {
            const academicYears = []
            return admin.firestore().collection("METADATA").listDocuments()
            .then(docList => {
                docList.forEach(doc => {
                    if(doc.id !== "institution")
                        academicYears.push(doc.id)
                })
                return academicYears
            }).catch(err => {
                throw err
            })
        }

        // adding is only possible for next academic year
        this.addNewSpecialization = (data) => {
            if(!data.course.type || !data.course.code || !data.specialization.name || !data.specialization.code)
                throw new Error("incorrect-parameters")

            let academicYear = ""
            return admin.firestore().collection("METADATA").doc("institution").get()
            .then(snapshot => {
                academicYear = snapshot.data().academicYear.split("-")
                academicYear = (parseInt(academicYear[0])+1) +"-"+ (parseInt(academicYear[1])+1)

                return admin.firestore().collection("METADATA").doc(academicYear).get()
            }).then(snapshot => {
                const courses = snapshot.data().courses
                if(!courses[data.course.type] && !Array.isArray(courses[data.course.type]))
                    throw new Error("not-found")
                courses[data.course.type] = courses[data.course.type].map(course => {
                    if(course.code === data.course.code){
                        let specializations = []
                        if(course.specializations && Array.isArray(course.specializations))
                            specializations = course.specializations.filter(branch => branch.code !== data.specialization.code)
                        specializations.push({
                            name : data.specialization.name,
                            code : data.specialization.code
                        })
                        course.specializations = specializations
                    }
                    return course
                })

                return admin.firestore().collection("METADATA").doc(academicYear).set({courses : courses},{merge:true})
                
            }).catch(err => {
                throw err
            })
        }

        // Can remove the specilization of next year only
        this.removeSpecilization = (data) => {
            if(!data.specialization.code || !data.course.type || !data.course.code)
                throw new Error("incorrect-parameters")

            let academicYear = ""
            return admin.firestore().collection("METADATA").doc("institution").get()
            .then(snapshot => {
                academicYear = snapshot.data().academicYear.split("-")
                academicYear = (parseInt(academicYear[0])+1) +"-"+ (parseInt(academicYear[1])+1)
                
                return admin.firestore().collection("METADATA").doc(academicYear).get()
            }).then(snapshot => {
                const courses = snapshot.data().courses

                courses[data.course.type] = courses[data.course.type].filter(course => {
                    if(course.code === data.course.code){
                        course.specializations = course.specializations.filter(specialization => specialization.code !== data.specialization.code)
                    }
                    return course
                })

                return admin.firestore().collection("METADATA").doc(academicYear).set({
                    courses : courses
                },{merge : true})
            }).catch(err => {
                throw err
            })
        }

        this.addNewCourse = (data) => {
            if(!data.course || !data.course.type || !data.course.name || !data.course.code || !data.course.years)
                throw new Error("incorrect-parameters")
            const courseCode = data.course.code
            const courseType = data.course.type
            const courseName = data.course.name
            const courseYears = data.course.years
            
            let academicYear = ""
            return admin.firestore().collection("METADATA").doc("institution").get()
            .then(snapshot => {
                if(!snapshot.exists)
                    throw new Error("not-found")
                academicYear = snapshot.data().academicYear.split("-")
                academicYear = (parseInt(academicYear[0])+1) +"-"+ (parseInt(academicYear[1])+1)
                
                return admin.firestore().collection("METADATA").doc(academicYear).get()
            }).then(snapshot => {
                let data = {courses : {}}
                if(snapshot.exists)
                    data = snapshot.data()
                if(!data.courses[courseType])
                    data.courses[courseType] = []
                data.courses[courseType] = data.courses[courseType].filter(course => course.code !== courseCode)
                data.courses[courseType].push({
                    code : courseCode,
                    specializations : [],
                    name : courseName,
                    years : courseYears
                })

                return admin.firestore().collection("METADATA").doc(academicYear).set(data)
            }).catch(err => {
                throw err
            })
        }

        // Can remove the course of next academic year only
        this.removeCourse = (data) => {
            if(!data.course.type || !data.course.code)
                throw new Error("incorrect-parameters")

            let academicYear = ""
            return admin.firestore().collection("METADATA").doc("institution").get()
            .then(snapshot => {
                academicYear = snapshot.data().academicYear.split("-")
                academicYear = (parseInt(academicYear[0])+1) +"-"+ (parseInt(academicYear[1])+1)
                
                return admin.firestore().collection("METADATA").doc(academicYear).get()
            }).then(snapshot => {
                const courses = snapshot.data().courses

                courses[data.course.type] = courses[data.course.type].filter(course => course.code !== data.course.code)

                return admin.firestore().collection("METADATA").doc(academicYear).set({
                    courses : courses
                },{merge : true})
            }).catch(err => {
                throw err
            })
        }
    
        this.mapStudentsToAcademics =  (data) => {
            const promises = []
            let docRef = academics
            data.academicYearList.map(academicYear => {
                const academicYearName = academicYear.name
                docRef = academics.doc(academicYearName)
                return academicYear.courseList.map(course => {
                    const courseName = course.name
                    docRef = docRef.collection(courseName)
                    return course.branchList.map(branch => {
                        const branchName = branch.name
                        docRef = docRef.doc(branchName)
                        return branch.yearList.map(year => {
                            const yearName = year.name
                            docRef = docRef.collection(yearName)
                            return year.semesterList.map(semester => {
                                const semesterName = semester.name
                                docRef = docRef.doc(semesterName)
                                const promise = docRef.get()
                                .then(doc => {
                                    let data = {}
                                    if(doc.exists)
                                        data = doc.data()
                                    if(!data.metadata)
                                        data.metadata = {}
                                    if(!data.metadata.serverTimestamp)
                                        data.metadata.serverTimestamp = []
                                    // console.log(data)
                                    const promise = docRef.set({
                                        ...semester.studentsList,
                                        metadata : {
                                            academicYear : academicYearName,
                                            course : courseName,
                                            branch : branchName,
                                            year : yearName,
                                            semester : semesterName,
                                            serverTimestamp : [...data.metadata.serverTimestamp,new Date().toUTCString()]
                                        }
                                    },{merge:true})
                                    promises.push(promise)
                                    return Promise.resolve(promise)
                                }).catch(err => {
                                    console.log(err)
                                });
                                promises.push(promise)
                                return Promise.resolve(promise)
                            })
                        })
                    })
                })
            })
            return Promise.all(promises)
        }
        
        this.mapSubjectsToAcademics = (data) => {
            const promises = []
            let docRef = academics
            data.academicYearList.map(academicYear => {
                const academicYearName = academicYear.name
                docRef = academics.doc(academicYearName)
                return academicYear.courseList.map(course => {
                    const courseName = course.name
                    docRef = docRef.collection(courseName)
                    return course.branchList.map(branch => {
                        const branchName = branch.name
                        docRef = docRef.doc(branchName)
                        return branch.yearList.map(year => {
                            const yearName = year.name
                            docRef = docRef.collection(yearName)
                            return year.semesterList.map(semester => {
                                const semesterName = semester.name
                                docRef = docRef.doc(semesterName)
                                const promise = docRef.get()
                                .then(doc => {
                                    let data = {
                                        metadata : {
                                            serverTimestamp : []
                                        }
                                    }
                                    if(doc.exists)
                                        data = doc.data()
                                    if(!data.metadata)
                                        data.metadata = {}
                                    if(!data.metadata.serverTimestamp)
                                        data.metadata.serverTimestamp = []
                                    // console.log(data)
                                    const promise = docRef.set({
                                        subjectsList : semester.subjectsList,
                                        metadata : {
                                            academicYear : academicYearName,
                                            course : courseName,
                                            branch : branchName,
                                            year : yearName,
                                            semester : semesterName,
                                            serverTimestamp : [...data.metadata.serverTimestamp,new Date().toUTCString()]
                                        }
                                    },{merge:true})
                                    promises.push(promise)
                                    return Promise.resolve(promise)
                                }).catch(err => {
                                    console.log(err)
                                });
                                promises.push(promise)
                                return Promise.resolve(promise)
                            })
                        })
                    })
                })
            })
            return Promise.all(promises)
        }

        this.listStudents = (data) => {

            const course = data.course || ""
            const branch = data.branch || ""
            const year = data.year || ""
            const semester = data.semester || ""
            const section = data.section || ""
            const academicYear = data.academicYear || ""
            
            const docRef = [admin.firestore().collection("ACADEMICS")]
            return admin.firestore().collection("ACADEMICS").listDocuments()
            .then(docsList => {
                const promises = []
                if(academicYear)
                    return Promise.all([admin.firestore().collection("ACADEMICS").doc(academicYear).listCollections()])
                docsList.forEach(doc => {
                    promises.push(doc.listCollections())
                })
                return Promise.all(promises)
            }).then(collectionList => {
                let promises = []
                collectionList.forEach(collection => {
                    if (course)
                        collection = collection.filter(collection => collection.id === course)
                    promises.push(...collection.map(collection => collection.listDocuments()))
                })
                return Promise.all(promises)
            }).then(docsList => {
                let promises = []
                docsList.forEach(doc => {
                    if (branch)
                        doc = doc.filter(doc => doc.id === branch)
                    promises.push(...doc.map(doc => doc.listCollections()))
                })
                return Promise.all(promises)
            }).then(collectionList => {
                let promises = []
                collectionList.forEach(collection => {
                    if (year)
                        collection = collection.filter(collection => collection.id === year)
                    promises.push(...collection.map(collection => collection.listDocuments()))
                })
                return Promise.all(promises)
            }).then(docsList => {
                let promises = []
                docsList.forEach(doc => {
                    if (semester)
                        doc = doc.filter(doc => doc.id === semester)
                    // console.log(doc)
                    promises.push(...doc.map(doc => doc.get()))
                })
                return Promise.all(promises)
            }).then(snapshotList => {
                let promises = []
                snapshotList.forEach(snapshot => {
                    const data = snapshot.data()
                    // console.log(data)
                    let users = []
                    if(section)
                        promises.push(...(new Set(data[section])))
                    else
                    for(let i in data){
                        if(i.length === 1){
                            users.push(...(new Set(data[i])))
                        }
                    }
                    promises.push(...(new Set(users)))
                })
                promises = [...(new Set(promises))]
                return Promise.all(promises)
            }).catch(err => {
                throw err
            })
        }
        
        this.duplicateStudentsToAcademics =  (data) => {
            if(!data.fromAcademics.academicYear || !data.fromAcademics.course || !data.fromAcademics.branch || !data.fromAcademics.year || !data.fromAcademics.semester)
                    throw new Error("incorrect-parameters")
                if(!data.toAcademics.academicYear || !data.toAcademics.course || !data.toAcademics.branch || !data.toAcademics.year || !data.toAcademics.semester)
                    throw new Error("incorrect-parameters")

            const fromPath = data.fromAcademics.academicYear + "/" + data.fromAcademics.course + "/" + data.fromAcademics.branch + "/" + data.fromAcademics.year + "/" + data.fromAcademics.semester
            const toPath = data.toAcademics.academicYear + "/" + data.toAcademics.course + "/" + data.toAcademics.branch + "/" + data.toAcademics.year + "/" + data.toAcademics.semester

            return academics.doc(fromPath).get()
            .then(doc => {
                let data = {}
                if(!doc.exists)
                    throw new Error("not-found")
                data = doc.data()
                return academics.doc(toPath).set(data)
            }).catch(err => {
                throw err
            })
        }

        this.addTimetable = (data) => {
            //  Need to work out
            if(!data.course || !data.branch || !data.year || !data.semester || !data.timetable)
                throw new Error("incorrect-parameters")
            if(!data.timetable.monday || !data.timetable.tuesday || !data.timetable.wednesday || !data.timetable.thursday || !data.timetable.friday || !data.timetable.saturday)
                throw new Error("incorrect-parameters")
            
            if(!Array.isArray(data.timetable.monday) || !Array.isArray(data.timetable.tuesday) || !Array.isArray(data.timetable.wednesday) || !Array.isArray(data.timetable.thursday) || !Array.isArray(data.timetable.friday) || !Array.isArray(data.timetable.saturday))
                throw new Error("incorrect-parameters")

            return admin.firestore().collection("METADATA").doc("institution").get()
            .then(snapshot => {
                    academicYear = snapshot.data().academicYear.split("-")
                    academicYear = (parseInt(academicYear[0])+1) +"-"+ (parseInt(academicYear[1])+1)
                return academics.doc(academicYear +"/"+ data.course +"/"+ data.branch +"/"+ data.year +"/"+ data.semester).set({
                    timetable : data.timetable
                },{merge:true})
            }).catch(err => {
                throw err
            })
        }

        this.addSubjectsoAcademics = (data) => {
            if(!data.course || !data.branch || !data.year || !data.semester || !data.subjects || !Array.isArray(data.subjects))
                throw new Error("incorrect-parameters")
                
            return admin.firestore().collection("METADATA").doc("institution").get()
            .then(snapshot => {
                    academicYear = snapshot.data().academicYear.split("-")
                    academicYear = (parseInt(academicYear[0])+1) +"-"+ (parseInt(academicYear[1])+1)
                return academics.doc(academicYear +"/"+ data.course +"/"+ data.branch +"/"+ data.year +"/"+ data.semester).set({
                    subjects : data.subjects
                },{merge:true})
            }).catch(err => {
                throw err
            })
        }

        this.getSubjects = (data) => {
            if(!data.course || !data.branch || !data.year || !data.semester)
                throw new Error("incorrect-parameters")
            return admin.firestore().collection("METADATA").doc("institution").get()
            .then(snapshot => {
                    academicYear = snapshot.data().academicYear.split("-")
                    academicYear = (parseInt(academicYear[0])+1) +"-"+ (parseInt(academicYear[1])+1)
                return academics.doc(academicYear +"/"+ data.course +"/"+ data.branch +"/"+ data.year +"/"+ data.semester).get()
            }).then(doc => {
                return doc.data()
            }).catch(err => {
                throw err
            })
        }

    }

}

module.exports = Academics