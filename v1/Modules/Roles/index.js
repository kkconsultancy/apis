const {admin} = require("../../../admin")
const roles = admin.firestore().collection("ROLES")

class Roles {

    constructor() {

        this.create = (data) => {
            if(!data.name || !data.priority || !data.uid)
                throw new Error("incorrect-parameters")

            const id = data.name.toLowerCase()
            const name = data.name.toUpperCase()
            const createdBy = data.uid

            return roles.where("id","==",id).get()
            .then(querySnapshot => {
                if(!querySnapshot.empty)
                    throw new Error("exists")

                return roles.doc(id).set({
                    id : id,
                    name : name,
                    createdBy : createdBy,
                    services : {},
                    serverTimestamp : new Date().toUTCString()
                })
            }).catch(err => {
                throw err
            })
        }

        this.map = data => {
            if(!data.types || !data.services || !Array.isArray(data.types) || !data.role)
                throw new Error("incorrect-parameters")
            let input = {}
            return roles.doc(data.role).get()
            .then(snapshot => {
                if(!snapshot.exists)
                    throw new Error("not-found")
                input = snapshot.data()
                if(!input.services)
                    input.services = {}
                const promises = []
                data.types.map(type => {
                    if(!Array.isArray(data.services[type]))
                        return type;

                    // eslint-disable-next-line
                    const promise = admin.firestore().collection("SERVICES").where("type","==",type).get()
                    .then(querySnapshot => {
                        if(querySnapshot.empty)
                            return querySnapshot;
                        querySnapshot.docs.forEach(doc => {
                            const inputServices = doc.data()
                            const name = inputServices.name
                            if(!Array.isArray(input.services[name]))
                                input.services[name] = []
                            let services = new Set([...input.services[name],...data.services[type]])
                            input.services[name] = [...services]
                            // return inputServices
                        })
                        return snapshot.docs
                    }).catch(err => {
                        console.log(err)
                    })
                    return promises.push(promise)
                    // return Promise.all([promise])
                })
                return Promise.all(promises)
            }).then(() => {
                // console.log(data.role,input)
                return roles.doc(data.role).set({...input})
            })
            .catch(err => {
                throw err
            })
            
        }

        this.assign = data => {
            // console.log(data)
            if(!Array.isArray(data.usersList) || !data.role)
                throw new Error("incorrect-parameters")
            
            return roles.doc(data.role).get()
            .then(snapshot => {
                if(!snapshot.exists)
                    throw new Error("not-found")
                const promises = data.usersList.map(user => {
                    return admin.firestore().collection("USERS").doc(user).get()
                })
                return Promise.all(promises)
            }).then(docList => {
                const promises = []
                docList.map(doc => {
                    // console.log(doc)
                    
                    if(!doc.exists)
                        return "";

                    const user = doc.data()
                    // console.log(user)
                    let roles = user.claims
                    if(roles)
                       roles = roles.roles
                    // console.log(roles)
                    if(Array.isArray(roles))
                        roles = new Set(roles)
                    else
                        roles = new Set()
                    // console.log(roles)
                        
                    roles.add(data.role)
                    // console.log(roles)

                    let claims = user.claims
                    claims.roles = [...roles]

                    const promise = admin.firestore().collection("USERS").doc(user.uid).set({claims : claims},{merge:true})

                    promises.push(promise)

                    return promise
                })
                return Promise.all(promises)
            })
            .catch(err => {
                throw err
            })
        }

        // Pending Functions

        this.activate = (data) => {
            if(!data.type || !data.service)
                throw new Error("incorrect-parameters")

            return roles.where("type","==",data.type).where("servicesList","array-contains",data.service).get()
            .then(querySnapshot => {
                if(querySnapshot.empty)
                    throw new Error("not-found")
                const promises = []
                return querySnapshot.docs.forEach(doc => {
                    const input = doc.data()
                    input.roles[data.service].active = true
                    
                    promises.push(roles.doc(input.name).set({...input}))
                })
            }).catch(err => {
                throw err
            })
        }

        this.deactivate = (data) => {
            if(!data.type || !data.service)
                throw new Error("incorrect-parameters")
            console.log(data)
            return roles.where("type","==",data.type).where("servicesList","array-contains",data.service).get()
            .then(querySnapshot => {
                if(querySnapshot.empty)
                    throw new Error("not-found")
                const promises = []
                return querySnapshot.docs.forEach(doc => {
                    const input = doc.data()
                    console.log(input)
                    input.roles[data.service].active = false
                    
                    promises.push(roles.doc(input.name).set({...input}))
                })
            }).catch(err => {
                throw err
            })
        }
    }

}

module.exports = Roles