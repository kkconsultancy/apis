const {admin} = require("../../../admin")
const services = admin.firestore().collection("SERVICES")

class Services {

    constructor() {

        this.activate = (data) => {
            if(!data.type || !data.service)
                throw new Error("incorrect-parameters")

            return services.where("type","==",data.type).where("servicesList","array-contains",data.service).get()
            .then(querySnapshot => {
                if(querySnapshot.empty)
                    throw new Error("not-found")
                const promises = []
                return querySnapshot.docs.forEach(doc => {
                    const input = doc.data()
                    input.services[data.service].active = true
                    
                    promises.push(services.doc(input.name).set({...input}))
                })
            }).catch(err => {
                throw err
            })
        }

        this.deactivate = (data) => {
            if(!data.type || !data.service)
                throw new Error("incorrect-parameters")
            console.log(data)
            return services.where("type","==",data.type).where("servicesList","array-contains",data.service).get()
            .then(querySnapshot => {
                if(querySnapshot.empty)
                    throw new Error("not-found")
                const promises = []
                return querySnapshot.docs.forEach(doc => {
                    const input = doc.data()
                    console.log(input)
                    input.services[data.service].active = false
                    
                    promises.push(services.doc(input.name).set({...input}))
                })
            }).catch(err => {
                throw err
            })
        }

        this.assign = (data) => {
            if(!data.user || !data.types || !Array.isArray(data.types) || !data.services || !Array.isArray(data.services))
                throw new Error("incorrect-parameters")

            const promises = []
            const claims = {}
            data.types.map(type => {
                const promise = services.where("type","==",type).where("servicesList","array-contains-any",data.services[type]).get()
                .then(querySnapshot => {
                    if(querySnapshot.size < 1)
                        throw new Error("not-found")
                    const output = []
                    querySnapshot.docs.forEach(doc => {
                        doc.data().servicesList.filter(service => data.services.indexOf(service) > -1 && doc.data().services[service].active).map(service => {
                            output.push(service)
                            return data.services.pop(data.service.indexOf(service))
                        })
                    })
                    return claims[type] = output
                }).catch(err => {
                    // continue
                    console.log(err)
                    claims[type] = []
                })
                return promises.push(promise)
                // return Promise.all([promise])
            })

            return Promise.all(promises)
            .then(() => {
                // console.log(claims)
                return admin.firestore().collection("USERS").doc(data.user).get()
            }).then(snapshot => {
                if(!snapshot.exists)
                    throw new Error("not-found")
                
                const user = snapshot.data()
                user.claims.services = {...user.claims,...claims}

                return admin.firestore().collection("USERS").doc(user.uid).set({
                    claims : {
                        services : user.claims.services
                    }
                })
            }).catch(err => {
                throw err
            })
            
        }
    }

}

module.exports = Services