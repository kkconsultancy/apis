const admin = require("firebase-admin")
admin.initializeApp({
    credential: admin.credential.cert(require('./json/v1/server_key.json')),
    databaseURL: "https://site-org-automation.firebaseio.com"
});

const auth = admin.auth();
async function removeAllUsers(nextPageToken) {
    // List batch of users, 1000 at a time.
    return admin.auth().listUsers(1000, nextPageToken)
        .then((listUsersResult) => {
            const promises = []
            listUsersResult.users.forEach((userRecord) => {
                // console.log('user', userRecord.toJSON());
                console.log(userRecord.uid + " DELETION INTIATED");
                promises.push(auth.deleteUser(userRecord.uid));
            });
            if (listUsersResult.pageToken) {
                // List next batch of users.
                promises.push(listAllUsers(listUsersResult.pageToken));
            }
            return Promise.all(promises);
        }).then(res => {
            return res//console.log("DONE")
        })
        .catch((error) => {
            return console.log('Error listing users:', error);
        });
}

const firestore = admin.firestore();
async function cleanCollections(col) {
    // console.log(col)
    return col.listCollections()
        .then(collections => {
            const promises = []
            // console.log(collections.values())
            collections.forEach(collection => {
                // console.log(collection.id)
                const promise = col.collection(collection.id).listDocuments()
                // console.log(promise)
                promises.push(promise)
                // console.log(promise)
                // const col = firestore.collection(collection.id)
                // getCollections(col)
            })
            // console.log(promises)
            return Promise.all(promises)
        }).then(res => {
            // console.log(res)
            return cleanFirestore(res)
        }).then(res => {
            return res
        }).catch(err => {
            console.log(err)
        })
}

async function cleanFirestore(documents) {

    // console.log(documents)
    const promises = []
    for (let doc in documents) {
        const d = documents[doc]
        // console.log(d)
        for (let i in d) {
            let promise = d[i].delete()
            promises.push(promise)
            promise = cleanCollections(d[i])
            promises.push(promise)
        }
        // promises.push(promise)
    }
    return Promise.all(promises)
}

const template = require('./json/v1/template.json')

function loadTemplate(template, firestore, promises, t) {
    // console.log(template)
    for (let collection in template) {
        for (let document in template[collection]) {
            if (document === 'isCollection')
                continue;
            const data = template[collection]
            const col = firestore.collection(collection).doc(document)
            if (data[document].isCollection) {
                // console.log(collection)
                loadTemplate(data[document], col, promises, t)
                // promises.push(promise)
            }
            else {
                data[document].timestamp = new Date().toUTCString()
                const promise = t.set(col, data[document])
                promises.push(promise)
                console.log(data[document], collection, document)
                // promises.push(promise)
            }
        }
    }
    console.log("COMPLETED LOADING TEMPLATE")
    // return Promise.all(promises)
    // return batch.commit()
}
return removeAllUsers().then(res => {
    // console.log(res)
    console.log("ACCOUNTS REMOVING DONE")
    return cleanCollections(admin.firestore())
}).then(res => {
        // console.log(res)
        console.log("CLEANING DONE")

        const promises = []
        return firestore.runTransaction(t => {
            loadTemplate(template, firestore, promises, t)
            return Promise.all(promises)
        })
}).then(res => {
    // console.log(res)
    return console.log("LOADING DONE")
}).then(res => {
    // console.log(res)
    return console.log("DATA LOADED")
}).catch(err => {
    console.log(err)
})
