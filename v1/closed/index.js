const {admin,closedAPI} = require("../../admin")

class ClosedAPI{
    constructor(){
        const app = closedAPI
        const responses = require('../../json/v1/responses.json')
        const Profiles = new (require("../Modules/Profile"))(admin)
        const Academics = new (require("../Modules/Academics"))()

        app.post('/getUid',(request,response) => {
            console.log(request.originalUrl)
            const type = request.body.type
            const data = request.body.data
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (uid !== originalUrlUid || !type || !data || !token || !uid){
                return response.status(200).send(responses["request-error"].general)
            }
            switch(type){
                case "phoneNumber":
                    if(!data.match(/^\+[0-9]{1,}[0-9]{10}$/))
                        return response.status(200).send(responses["request-error"].general)
                    break;
                case "email":
                    if (!data.match(/^.+@.+\..+$/))
                        return response.status(200).send(responses["request-error"].general)
                    break;
                default:
                    return response.status(200).send(responses["request-error"].general)       
            }

            return admin.auth().verifyIdToken(token)
            .then(res => {
                if (res.user_id !== uid)
                    throw new Error('auth')
                return admin.firestore().collection('USERS').where(type+"List","array-contains",data).get()
            }).then(dbSnapshot => {
                if(dbSnapshot.size < 1)
                    return response.send({ uidList : [] })

                const uids = []
                dbSnapshot.forEach(doc => {
                    const data = doc.data()
                    // console.log(data)
                    return uids.push(data.uid)
                })
                return response.send({ uidList : uids, branding : responses.branding })
            })
            .catch(err => {
                console.log(err)
                if (err.toString().split('\n')[0] === 'Error: auth' || err.toString().match("Decoding Firebase ID token failed")) {
                    // console.log('user id wrong')
                    return response.status(200).send(responses["auth-error"].general)
                }
                return response.status(200).send(responses["server-error"].general)
            })

        });

        app.post('/getUser',(request,response) => {
            console.log(request.originalUrl)
            const uid = request.body.uid
            const token = request.body.token
            const user = request.body.username
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (uid !== originalUrlUid || !uid || !user || !token){
                return response.status(200).send(responses["request-error"].general)
            }
            return admin.auth().verifyIdToken(token)
            .then(res => {
                if (res.uid !== uid)
                    throw new Error('auth')
                return admin.firestore().collection('USERS').doc(user).get()
            })
            .then(doc => {
                if (!doc.exists)
                    return response.send({ uidList: [] })
                const data = doc.data()
                data.branding = responses.branding
                return response.send(data)
            })
            .catch(err => {
                console.log(err)
                if (err.toString().split('\n')[0] === 'Error: auth' || err.toString().match("Decoding Firebase ID token failed")) {
                    // console.log('user id wrong')
                    return response.status(200).send(responses["auth-error"].general)
                }
                return response.status(200).send(responses["server-error"].general)
            })
        });

        app.post('/getServices', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (!uid || !token || uid !== originalUrlUid) {
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    token_info.roles = token_info.roles || []
                    token_info.services = token_info.services || {}
                    return token_info
                }).then( claims => {
                    let promises = []
                    for(let i in token_info.roles){
                        const role = token_info.roles[i]
                        // console.log(role)
                        const promise = admin.firestore().collection('ROLES').doc(role).get()
                        promises.push(promise)
                    }
                    return Promise.all(promises)
                }).then(res => {
                    const services = {}
                    // console.log(res)
                    for (let i in res) {
                        if (res[i].exists) {
                            const servicesList = res[i].data().services
                            // console.log(servicesList)
                            for (let s in servicesList) {
                                services[s] = servicesList[s]
                            }
                        }
                    }
                    token_info.services = { ...services, ...token_info.services }
                    // console.log(token_info.services)
                    return token_info.services
                }).then(servicesList => {
                    const promises = []
                    for(let i in servicesList){
                        // console.log(i)
                        const promise = admin.firestore().collection('SERVICES').doc(i).get()
                        promises.push(promise)
                    }
                    return Promise.all(promises)
                }).then(servicesList => {
                    const servicesFinalList = []
                    // console.log(servicesList)
                    servicesList.map(service => {
                        // console.log(service)
                        if(!service.exists)
                            return [];
                        const data = service.data()
                        // console.log(data)
                        let services = data.services
                        token_info.services[service.id] = [...(new Set(token_info.services[service.id]))]
                        // console.log(token_info.services[service.id],services,service.id)
                        const output = token_info.services[service.id].filter(s => services[s] && services[s].active).map(s => services[s])
                        return servicesFinalList.push(...output)
                        // console.log(servicesFinalList)
                    })
                    return token_info.services = servicesFinalList
                })
                .then(res => {
                    // console.log(token_info.services)
                    token_info.roles = [...(new Set(token_info.roles))]
                    return response.send({ services: token_info.services, roles: token_info.roles })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/getProfile', (request, response) => {
            console.log(request.originalUrl)
            const uid = request.body.uid
            const token = request.body.token
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (uid !== originalUrlUid || !uid || !token) {
                return response.status(200).send(responses["request-error"].general)
            }
            return admin.auth().verifyIdToken(token)
            .then(res => {
                if (res.uid !== uid)
                    throw new Error('auth')
                // return admin.firestore().collection('PROFILES').doc(uid).get()
                return Profiles.view(uid)
            })
            .then(data => {
                // if (!doc.exists)
                //     return response.send({ uidList: [] })
                // const data = Profiles.view(uid)
                data.branding = responses.branding
                return response.send(data)
            })
            .catch(err => {
                console.log(err)
                if (err.toString().split('\n')[0] === 'Error: auth' || err.toString().match("Decoding Firebase ID token failed")) {
                    // console.log('user id wrong')
                    return response.status(200).send(responses["auth-error"].general)
                } if (err.toString().match("not-found")) {
                    // console.log('user id wrong')
                    return response.status(200).send(responses["data-not-found-error"].users)
                }
                
                return response.status(200).send(responses["server-error"].general)
            })
        });

        app.post('/updatePrimaryEmail', (request, response) => {
            console.log(request.originalUrl)
            const uid = request.body.uid
            const token = request.body.token
            const email = request.body.email
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (uid !== originalUrlUid || !uid || !token || !email) {
                return response.status(200).send(responses["request-error"].general)
            }
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.uid !== uid)
                        throw new Error('auth')
                    // return admin.firestore().collection('PROFILES').doc(uid).get()
                    return Profiles.updatePrimaryEmail(uid,email)
                })
                .then(data => {
                    // if (!doc.exists)
                    //     return response.send({ uidList: [] })
                    // const data = Profiles.view(uid)
                    data = {
                        "updation" : true
                    }
                    data.branding = responses.branding
                    return response.send(data)
                })
                .catch(err => {
                    console.log(err)
                    if (err.toString().split('\n')[0] === 'Error: auth' || err.toString().match("Decoding Firebase ID token failed") || err.toString().match("Firebase ID token has expired")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    } if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    } if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].email)
                    }

                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/updatePrimaryPhoneNumber', (request, response) => {
            console.log(request.originalUrl)
            const uid = request.body.uid
            const token = request.body.token
            const phoneNumber = request.body.phoneNumber
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (uid !== originalUrlUid || !uid || !token || !phoneNumber) {
                return response.status(200).send(responses["request-error"].general)
            }
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.uid !== uid)
                        throw new Error('auth')
                    // return admin.firestore().collection('PROFILES').doc(uid).get()
                    return Profiles.updatePrimaryPhoneNumber(uid, phoneNumber)
                })
                .then(data => {
                    // if (!doc.exists)
                    //     return response.send({ uidList: [] })
                    // const data = Profiles.view(uid)
                    data = {
                        "updation": true
                    }
                    data.branding = responses.branding
                    return response.send(data)
                })
                .catch(err => {
                    console.log(err)
                    if (err.toString().split('\n')[0] === 'Error: auth' || err.toString().match("Decoding Firebase ID token failed") || err.toString().match("Firebase ID token has expired")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    } if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    } if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].phoneNumber)
                    }

                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/requestResetPassword', (request, response) => {
            console.log(request.originalUrl)
            const uid = request.body.uid
            const token = request.body.token
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (uid !== originalUrlUid || !uid || !token) {
                return response.status(200).send(responses["request-error"].general)
            }
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.uid !== uid)
                        throw new Error('auth')
                    return Profiles.generatePasswordResetLink(uid)
                })
                .then(data => {
                    console.log(data)
                    data = {
                        status : true
                    }
                    data.branding = responses.branding
                    return response.send(data)
                })
                .catch(err => {
                    console.log(err)
                    if (err.toString().split('\n')[0] === 'Error: auth' || err.toString().match("Decoding Firebase ID token failed") || err.toString().match("Firebase ID token has expired")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    } if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    } if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].email)
                    }

                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/getCustomToken', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]
            let roles = []
            let services = {}
            if(request.body.claims){
                roles = request.body.claims.roles
                if(!Array.isArray(roles))
                    roles = []
                services = request.body.claims.services || {}
            }
            

            if (uid !== originalUrlUid || !uid || !token) {
                return response.status(200).send(responses["request-error"].general)
            }

            const finalClaims = {
                roles : roles,
                services : services
            }
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    return admin.firestore().collection('USERS').doc(uid).get()
                }).then(doc => {
                    console.log(doc)
                    if (!doc.exists)
                        throw new Error('auth')
                    const data = doc.data()
                    // console.log(data.claims)
                    return data.claims ? data.claims : {}
                }).then(claims => {
                    // const updatedClaims = {roles : claims.roles}
                    finalClaims.roles = [...claims.roles,...finalClaims.roles] || []
                    finalClaims.services = {...claims.services,...finalClaims.services} || {}
                    // const newServices = new Set(...claims.roles)
                    const promises = []
                    for(let i in claims.roles){
                        // console.log(claims.roles[i])
                        promises.push(admin.firestore().collection("ROLES").doc(claims.roles[i].toString()).get())
                    }
                    return Promise.all(promises)
                }).then(services => {
                    // const servicesSet = new Set(finalClaims.services)
                    for(let i in services){
                        if(!services[i].exists)
                            continue;
                        const servicesList = services[i].data().services
                        // if(service)
                        //     servicesSet.add(service)
                        for (let service in servicesList){
                            // console.log(service)
                            if (!finalClaims.services[service])
                                finalClaims.services[service] = []
                            finalClaims.services[service] = [...servicesList[service], ...finalClaims.services[service]]
                        }
                        // console.log(service)
                    }
                    // finalClaims.services = servicesSet
                    return finalClaims
                }).then(claims => {
                    // console.log(claims)
                    claims.user_id = uid
                    return admin.auth().createCustomToken(uid, claims)
                }).then(token => {
                    return response.send({ token: token, branding : responses.branding })
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/verifyCustomToken', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            if (uid !== originalUrlUid || !uid || !token) {
                return response.status(200).send(responses["request-error"].general)
            }

            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    return response.send(res)
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/getForm', (request, response) => {
            console.log(request.originalUrl)
            const token = request.body.token
            const uid = request.body.uid
            const originalUrlUid = request.baseUrl.split('/').slice(-1)[0]

            let version = request.body.version
            if (!version)
                version = "DEFAULT"
            const category = request.body.category
            const type = request.body.type

            if (!uid || !token || uid !== originalUrlUid || !category || !type || !version) {
                // console.log(request)
                return response.status(200).send(responses["request-error"].general)
            }
            var token_info = {}
            var roles = []
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.user_id !== uid)
                        throw new Error('auth')
                    token_info = res
                    return admin.firestore().collection('USERS').doc(uid).get()
                    // return response.send({ res: res })
                }).then(doc => {
                    if (!doc.exists)
                        throw new Error('auth')
                    const data = doc.data()
                    // console.log(data.claims)
                    return data.claims ? data.claims : {}
                }).then(claims => {
                    // console.log(claims)
                    // console.log('FORMS/' + category + "/" + type + "/" + version)
                    return admin.firestore().collection('FORMS/' + version + "/" + category).doc(type).get()
                }).then(res => {
                    const data = res.data()
                    // console.log(data)
                    return response.send(data.fields)
                }).catch(err => {
                    console.log(err)
                    if (err.toString().match("auth") || err.toString().match("Firebase ID token")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    }
                    return response.status(200).send(responses["server-error"].general)
                })
        });

        // Photo URL cannot be tested via postman
        app.post('/updatePhotoURL', (request, response) => {
            console.log(request.originalUrl)
            const uid = request.body.uid
            const token = request.body.token
            const storageSourcePath = request.body.storageSourcePath
            // console.log(request.body)

            if (!uid || !token || !storageSourcePath) {
                return response.status(200).send(responses["request-error"].general)
            }
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.uid !== uid)
                        throw new Error('auth')
                    return Profiles.updatePhotoURL(uid, storageSourcePath)
                })
                .then(() => {
                    const data = {
                        status : true
                    }
                    data.branding = responses.branding
                    return response.send(data)
                })
                .catch(err => {
                    console.log(err)
                    if (err.toString().split('\n')[0] === 'Error: auth' || err.toString().match("Decoding Firebase ID token failed") || err.toString().match("Firebase ID token has expired")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    } if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    } if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].email)
                    }

                    return response.status(200).send(responses["server-error"].general)
                })
        });

        app.post('/getAcademicYears', (request, response) => {
            console.log(request.originalUrl)
            const uid = request.body.uid
            const token = request.body.token
            // console.log(request.body)

            if (!uid || !token) {
                return response.status(200).send(responses["request-error"].general)
            }
            return admin.auth().verifyIdToken(token)
                .then(res => {
                    if (res.uid !== uid)
                        throw new Error('auth')
                    return Academics.listAcademicYears()
                })
                .then((academicYears) => {
                    const data = {
                        academicYears : academicYears
                    }
                    data.branding = responses.branding
                    return response.send(data)
                })
                .catch(err => {
                    console.log(err)
                    if (err.toString().split('\n')[0] === 'Error: auth' || err.toString().match("Decoding Firebase ID token failed") || err.toString().match("Firebase ID token has expired")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["auth-error"].general)
                    } if (err.toString().match("not-found")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-not-found-error"].users)
                    } if (err.toString().match("exists")) {
                        // console.log('user id wrong')
                        return response.status(200).send(responses["data-exists"].email)
                    }

                    return response.status(200).send(responses["server-error"].general)
                })
        });

    }

}

module.exports = ClosedAPI;