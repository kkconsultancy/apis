const {admin} = require("../../../admin")

const attendace = admin.firestore().collection("ATTENDANCE")

class Attendance {

    constructor(firebase) {
        this.attendance = admin.firestore().collection('ATTENDANCE')
        const responses = require('../../../json/v1/responses.json')

        this.add = (data) => {
            // console.log(data)
            if(!data.academicYear || !data.course || !data.branch || !data.year || !data.semester || !data.date.month || !data.date.day || !data.date.year)
                throw new Error("incorrect-parameters")

            if (!data.periods || !data.section)
                throw new Error("incorrect-parameters")

            let path = data.academicYear + "/" + data.course + "/" + data.branch + "/" + data.year + "/" + data.semester + "/"
            path += data.date.month + "/" + data.date.day + "/" + data.section

            // console.log(path)

            const metadata = {
                academicYear : data.academicYear,
                course : data.course,
                branch : data.branch,
                year : data.year,
                semester : data.semester,
                date : data.date,
                subject : {
                    code : "",
                    name : ""
                },
                serverTimestamp : new Date().toUTCString(),
                section : data.section
            }
            let attendanceRef = attendace.doc(data.academicYear).collection(data.course).doc(data.branch).collection(data.year)
            attendanceRef = attendanceRef.doc(data.semester).collection(data.date.month).doc(data.date.day).collection(data.section)

            const promises = []
            for(let i in data.periods){
                metadata.period = i
                promises.push(attendanceRef.doc(i).set({
                    absent : data.periods[i],
                    metadata : {...metadata}
                }))
            }

            return Promise.all(promises)
        }

        this.view = (data) => {
            
            let academicYear = data.academicYear || ""
            let course = data.course || ""
            let branch = data.branch || ""
            let year = data.year || ""
            let semester = data.semester || ""
            let month = data.month || ""
            let day = data.day || ""
            let section = data.section || ""
            let period = data.period || ""
            let subject = data.subject || ""
            let students = Array.isArray(data.students) ? data.students : ""

            const promises = []
            
            return this.attendance.listDocuments()
            .then(docsList => {
                if(academicYear)
                    return Promise.all([this.attendance.doc(academicYear).listCollections()])
                docsList.forEach(doc => {
                    promises.push(doc.listCollections())
                })
                return Promise.all(promises)
            }).then(collectionList => {
                let promises = []
                collectionList.forEach(collection => {
                    if (course)
                        collection = collection.filter(collection => collection.id === course)
                    promises.push(...collection.map(collection => collection.listDocuments()))
                })
                return Promise.all(promises)
            }).then(docsList => {
                let promises = []
                docsList.forEach(doc => {
                    if (branch)
                        doc = doc.filter(doc => doc.id === branch)
                    promises.push(...doc.map(doc => doc.listCollections()))
                })
                return Promise.all(promises)
            }).then(collectionList => {
                let promises = []
                collectionList.forEach(collection => {
                    if (year)
                        collection = collection.filter(collection => collection.id === year)
                    promises.push(...collection.map(collection => collection.listDocuments()))
                })
                return Promise.all(promises)
            }).then(docsList => {
                let promises = []
                docsList.forEach(doc => {
                    if (semester)
                        doc = doc.filter(doc => doc.id === semester)
                    // console.log(doc)
                    promises.push(...doc.map(doc => doc.listCollections()))
                })
                return Promise.all(promises)
            }).then(collectionList => {
                let promises = []
                collectionList.forEach(collection => {
                    if (month)
                        collection = collection.filter(collection => collection.id === month)
                    promises.push(...collection.map(collection => collection.listDocuments()))
                })
                return Promise.all(promises)
            }).then(docsList => {
                let promises = []
                docsList.forEach(doc => {
                    // console.log(doc)
                    if (day)
                        doc = doc.filter(doc => doc.id === day)
                    promises.push(...doc.map(doc => doc.listCollections()))
                })
                return Promise.all(promises)
            }).then(collectionList => {
                // console.log(collectionList)
                let promises = []
                collectionList.forEach(collection => {
                    if (section)
                        collection = collection.filter(collection => collection.id === section)
                    if (subject)
                        collection = collection.map(collection => collection.where("metadata.subject.code", "==", subject))
                    if (students)
                        collection = collection.map(collection => collection.where("absent", "array-contains-any", students))

                    // Limitation by using get when fetching huge data
                    promises.push(...collection.map(collection => collection.get()))
                })
                return Promise.all(promises)
            }).then(querySnapshot => {
                const promises = []
                querySnapshot.forEach(snapshot => {
                    // console.log(snapshot)
                    promises.push(snapshot.docs)
                })
                return Promise.all(promises)
            }).then(docsList => {
                // console.log(docsList)
                let promises = []
                docsList.forEach(doc => {
                    // console.log(doc)
                    if (period)
                        doc = doc.filter(doc => doc.id === period)
                    promises.push(...doc)
                })
                return Promise.all(promises)
            }).then(snapshot => {
                // console.log(snapshot)
                return snapshot.map(doc => doc.data())
            }).then(result => {
                // console.log(result)
                return result
            })
            .catch(err => {
                throw err
            })
        }

        this.get = (data) => {
            // console.log(data)
            if(!data.uid || !data.academicYear)
                throw new Error("incorrect-parameters")
            let user = {}
            return admin.firestore().collection("USERS").doc(data.uid).get()
            .then(doc => {
                if(!doc.exists)
                    throw new Error("not-found")
                user = doc.data()
                if(!user.academics)
                    throw new Error("not-found")
            return attendace.doc(data.academicYear).collection(user.academics.course+"/"+user.academics.branch+"/"+user.academics.year).doc(user.academics.semester).listCollections()
            }).then(collectionList => {
                if(data.month)
                    collectionList = collectionList.filter(collection => collection.id === data.month)
                const promises = collectionList.map(collection => {
                    return collection.listDocuments()
                })
                return Promise.all(promises)
            }).then(docList => {
                if(data.day)
                    docList = docList.filter(doc => doc.id === data.day)
                const promises = []
                docList.map(doc => {
                    const subPromise = doc.map(doc => {
                        // console.log(doc)
                        if(user.academics.section)
                            doc = doc.collection(user.academics.section)
                        else
                            return Promise.reject(doc)
                        if(data.period)
                            doc = doc.where("period","==",data.period)
                        else
                            doc = doc.listDocuments()
                        return doc
                    })
                    return promises.push(...subPromise)
                })
                return Promise.all(promises)
            }).then(docList => {
                const promises = []
                docList.map(doc => {
                    const subPromises = doc.map(doc => {
                        // console.log(doc)
                        return doc.get()
                    })
                    return promises.push(...subPromises)
                })
                return Promise.all(promises)
            }).then(snapshotList => {
                const subjects = []
                // snapshotList.map(doc => console.log(doc.data()))
                snapshotList.filter(doc => doc.data().absent.filter(uid => uid === data.uid).length > 0).map(snapshot => {
                    const data = snapshot.data().metadata
                    // console.log(snapshot.data())
                    // if(!data.subject || !data.subject.name)
                    //     data.subject = {name : data.period,code:data.period}
                    // if(!subjects[data.subject.name])
                    //     subjects[data.subject.name] = 0    
                    // subjects[data.subject.name] += 1
                    subjects.push(data)
                    return data
                })
                // console.log(subjects)
                return subjects
            })
            .catch(err => {
                throw err
            })
        }

        // this.get({uid : "16K61A05G7",academicYear:"2019-2020"})
    }

}

module.exports = Attendance