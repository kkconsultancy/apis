const {admin} = require("../../../admin")
const transport = admin.firestore().collection("TRANSPORT")

class Transport{

    constructor(){

        this.create = (data) => {
            // Need to manage
        }

        this.add = (data) => {
            if(!data.academicYear || !data.course || !data.branch || !data.year)
                throw new Error("incorrect-parameters")
            if(!data.studentsList || !Array.isArray(data.studentsList))
                throw new Error("incorrect parameters")

            const promises = []
            const transportRef = transport.doc(data.academicYear).collection(data.course+"/"+data.branch+"/"+data.year)
            return admin.firestore().runTransaction(t => {
                data.studentsList.map(student => {
                    const studentData = {
                        contact : {
                            address : student.address || "",
                            phoneNumber : student.phoneNumber || ""
                        },bus : {
                            number : student.bus_number || "",
                            route : student.bus_route || "",
                            destination : student.bus_destination || ""
                        },
                        metadata : {
                            course : data.course,
                            academicYear : data.academicYear,
                            branch : data.branch,
                            year : data.year,
                            serverTimestamp : new Date().toUTCString()
                        },
                        uid : student.uid
                    }
                    const promise = t.set(transportRef.doc(student.uid),studentData)
                    promises.push(promise)

                    return student
                })
    
                return Promise.all(promises)
            })
        }
    }
    
}

module.exports = Transport