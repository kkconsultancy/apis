const {admin} = require("../../../admin")
const marks = admin.firestore().collection("MARKS")

class Marks {

    constructor(firebase) {
        
        this.add = (data) => {
            if(!data.academicYear || !data.course || !data.branch || !data.year || !data.semester)
                throw new Error("incorrect-parameters")

            if(!data.marksList || !Array.isArray(data.marksList) || !data.category || !data.phase || !data.type.name || !data.type.code || !data.maxScore)
                throw new Error("incorrect-parameters")
            const promises = []

            const marksRef = marks.doc(data.academicYear).collection(data.course+"/"+data.branch+"/"+data.year+"/"+data.semester+"/"+data.category)

            for(let i = 0;i < data.marksList.length;i += 499){
                // console.log(data.marksList)
                const promise = admin.firestore().runTransaction(t => {
                    // console.log(data.marksList,"yes")
                    data.marksList.slice(i,499).map(marks => {
                        const inputData = {
                            [data.type.code] : {
                                type : {
                                    name : data.type.name,
                                    code : data.type.code
                                },
                                marks : {
                                    [data.phase] : {
                                        scores : marks.scores,
                                        max : data.maxScore,
                                        metadata : {
                                            academicYear : data.academicYear,
                                            course : data.course,
                                            branch : data.branch,
                                            year : data.year,
                                            semester : data.semester,
                                            category : data.category,
                                            type : data.type,
                                            serverTimestamp : new Date().toUTCString()
                                        }
                                    }
                                },
                            },
                            uid : marks.uid,
                            category : data.category
                        }

                        const promise = t.set(marksRef.doc(marks.uid),inputData,{merge:true})
                        promises.push(promise)
                        return marks
                    })
                    // console.log(promises)
                    // return Promise.all(promises)
                    return Promise.resolve(promises)
                })

                promises.push(promise)
            }
            // console.log(promises)
            return Promise.all(promises)
            
        } 


        this.view = (data) => {
            
            let academicYear = data.academicYear || ""
            let course = data.course || ""
            let branch = data.branch || ""
            let year = data.year || ""
            let semester = data.semester || ""
            let category = data.category || ""
            let type = data.type || ""
            let phase = data.phase || ""
            // let types = Array.isArray(data.types) ? data.type : ""
            // let phases = Array.isArray(data.phases) && types ? data.phases : ""
            let students = Array.isArray(data.students) ? data.students : ""
            // console.log(data)
            const promises = []
            
            return marks.listDocuments()
            .then(docsList => {
                if(academicYear)
                    return Promise.all([marks.doc(academicYear).listCollections()])
                docsList.forEach(doc => {
                    promises.push(doc.listCollections())
                })
                return Promise.all(promises)
            }).then(collectionList => {
                // console.log(collectionList)
                let promises = []
                collectionList.forEach(collection => {
                    
                    if (course)
                        collection = collection.filter(collection => collection.id === course)
                    // console.log(collection)
                    promises.push(...collection.map(collection => collection.listDocuments()))
                })
                // console.log(promises)
                return Promise.all(promises)
            }).then(docsList => {
                let promises = []
                docsList.forEach(doc => {
                    // console.log(doc)
                    if (branch)
                        doc = doc.filter(doc => doc.id === branch)
                    promises.push(...doc.map(doc => doc.listCollections()))
                })
                return Promise.all(promises)
            }).then(collectionList => {
                let promises = []
                collectionList.forEach(collection => {
                    if (year)
                        collection = collection.filter(collection => collection.id === year)
                    promises.push(...collection.map(collection => collection.listDocuments()))
                })
                return Promise.all(promises)
            }).then(docsList => {
                let promises = []
                docsList.forEach(doc => {
                    if (semester)
                        doc = doc.filter(doc => doc.id === semester)
                    // console.log(doc)
                    promises.push(...doc.map(doc => doc.listCollections()))
                })
                return Promise.all(promises)
            }).then(collectionList => {
                let promises = []
                collectionList.forEach(collection => {
                    if (category)
                        collection = collection.filter(collection => collection.id === category)
                    let collections = []
                    if(students)
                        students.map(student => {
                            return collections.push(...collection.map(collection => collection.where(uid, "==", student)))
                        })
                    if(collections.length > 0)
                        collection = collections
                    collections = collection
                    if(type)
                        collection = collections.map(collection => {
                            // console.log(collection.id)
                            if(phase)
                                collection = collection.where(type+".phases","array-contains",phase)
                            return collection.where(type + ".type.code","==",type)
                        })
                    return promises.push(...collection.map(collection => collection.get()))
                })
                return Promise.all(promises)
            }).then(snapshot => {
                // console.log(snapshot)
                const promises = []
                snapshot.filter(snapshot => snapshot.size > 0).map(querySnapshot => querySnapshot.docs.filter(doc => doc.exists)
                .map(doc => {
                    let data = doc.data()                      
                    return promises.push(data)
                }))
                return Promise.all(promises)
            }).then(result => {
                // console.log(result)
                return result
            })
            .catch(err => {
                console.log(err)
                throw err
            })
        }

        
        this.get = (data) => {
            if(!data.uid || !data.academicYear)
                throw new Error("incorrect-parameters")
            let user = {}
            return admin.firestore().collection("USERS").doc(data.uid).get()
            .then(doc => {
                if(!doc.exists)
                    throw new Error("not-found")
                user = doc.data()
                if(!user.academics)
                    throw new Error("not-found")
            return marks.doc(data.academicYear).collection(user.academics.course+"/"+user.academics.branch+"/"+user.academics.year).doc(user.academics.semester).listCollections()
            }).then(collectionList => {
                if(data.category)
                    collectionList = collectionList.filter(collection => collection.id === data.category)
                const promises = collectionList.map(collection => {
                    return collection.where("uid","==",data.uid).get()
                })
                return Promise.all(promises)
            }).then(snapshotList => {
                // console.log(snapshotList)
                const marks = {}
                snapshotList.map(querySnapshot => {
                    const docs = querySnapshot.docs
                    return docs.map(doc => {
                        const data = doc.data()
                        // console.log(data)
                        if(!marks[data.type])
                            marks[data.type] = []
                        return marks[data.type].push(data)
                    })
                })
                
                // console.log(marks)
                return marks
            })
            .catch(err => {
                throw err
            })
        }

        // this.get({uid:"16K61A05G7",academicYear:"2018-2019"})

    }

}

module.exports = Marks