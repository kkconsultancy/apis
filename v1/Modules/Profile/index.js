const {admin,nodemailer} = require("../../../admin")

class Profile{

    constructor(firebase){
        this.admin = admin
        this.profiles = firebase.firestore().collection('PROFILES')
        this.users = firebase.firestore().collection('USERS')

        const responses = require('../../../json/v1/responses.json')

        this.view = (uid) => {
            return this.profiles.doc(uid).get()
            .then(res => {
                if(res.exists)
                    return res.data()
                throw new Error("not-found")
            }).catch(err => {
                throw err
            })
        }

        this.updatePrimaryEmail = (uid,primaryEmail) => {
            let existing = "";
            return this.profiles.where("email", "==", primaryEmail).where("accountCreated","==",true).get()
            .then(docs => {
                if(docs.size !== 0)
                    throw new Error("exists")
                return this.profiles.doc(uid).get()
            }).then(res => {
                if(res.exists){
                    existing = res.data().email
                    if(existing === primaryEmail)
                        throw new Error("exists")
                    return this.profiles.doc(uid).set({
                        email : primaryEmail.toLowerCase()
                    },{merge : true})
                }
                throw new Error("not-found")
            }).then(res => {
                return this.users.doc(uid).get()
            }).then(res => {
                let emailList = res.data().emailList.filter(email => email !== existing)
                emailList.push(primaryEmail.toLowerCase())
                return this.users.doc(uid).set({
                    emailList : emailList
                },{merge : true})
            }).then(res => {
                return this.admin.auth().updateUser(uid,{
                    email : primaryEmail.toLowerCase(),
                    emailVerified : false
                })
            }).catch(err => {
                throw err
            })
        }

        this.generatePasswordResetLink = (uid) => {
            const users = []
            let resetLink = ""
            return this.profiles.where("uid","==",uid).where("accountCreated","==",true).get()
            .then(snapshot => {
                if(snapshot.size < 1)
                    throw new Error("not-found")
                snapshot.forEach(user => {
                    users.push(user.data())
                })
                // console.log(users[0].email)
                return this.admin.auth().generatePasswordResetLink(users[0].email)
            }).then(link => {
                resetLink = link
                var mailOptions = {
                    from: '"AAA Automation" <no-reply@aaa.automation>',
                    to: users[0].displayName+', '+users[0].email,
                    subject: 'AAA Automation password reset link for '+users[0].uid,
                    text: 'Hey there, it’s time to reset your password ;) ',
                    html: '<b>Hey there! </b><br/> '+link
                };
                return nodemailer.sendMail(mailOptions)
            }).then(() => {
                return resetLink
            })
            .catch(err => {
                throw err
            })
            
        }

        this.updatePrimaryPhoneNumber = (uid, phoneNumber) => {
            let existing = "";
            return this.profiles.where("phoneNumber", "==", phoneNumber).where("accountCreated", "==", true).get()
                .then(docs => {
                    if (docs.size !== 0)
                        throw new Error("exists")
                    return this.profiles.doc(uid).get()
                }).then(res => {
                    if (res.exists) {
                        existing = res.data().phoneNumber
                        if (existing === phoneNumber)
                            throw new Error("exists")
                        return this.profiles.doc(uid).set({
                            phoneNumber: phoneNumber
                        }, { merge: true })
                    }
                    throw new Error("not-found")
                }).then(res => {
                    return this.users.doc(uid).get()
                }).then(res => {
                    let phoneNumberList = res.data().phoneNumberList.filter(phoneNumber => phoneNumber !== existing)
                    phoneNumberList.push(phoneNumber)
                    return this.users.doc(uid).set({
                        phoneNumberList: phoneNumberList,
                        phoneNumber : phoneNumber
                    }, { merge: true })
                }).then(res => {
                    return this.admin.auth().updateUser(uid, {
                        phoneNumber: phoneNumber
                    })
                }).catch(err => {
                    throw err
                })
        } 

        this.updatePhotoURL = (uid, sourcePath) => {
            let photoURL = ""
            return this.profiles.doc(uid).get()
                .then(docs => {
                    if (!docs.exists)
                        throw new Error("not-found")
                    let date = new Date().toLocaleDateString().split("/")
                    date[2] = parseInt(date[2]) + 10
                    return this.admin.storage().bucket().file(sourcePath).getSignedUrl({
                        action: "read",
                        expires: date.join("-")
                    })
                }).then(url => {
                    // console.log(url)
                    photoURL = url[0]
                    return this.profiles.doc(uid).set({
                        photoURL: photoURL
                    }, { merge: true })
                }).then(res => {
                    return this.users.doc(uid).get()
                }).then(res => {
                    return this.users.doc(uid).set({
                        photo: {
                            sourcePath: sourcePath,
                            photoURL: photoURL
                        }
                    }, { merge: true })
                }).then(res => {
                    return this.admin.auth().updateUser(uid, {
                        photoURL : photoURL
                    })
                }).catch(err => {
                    throw err
                })
        }

    }
    
}

module.exports = Profile